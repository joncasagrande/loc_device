package com.joncasagrande.myapplication

import android.app.Activity
import android.app.ActivityManager
import android.app.Dialog
import android.app.Service
import android.app.usage.UsageStats
import android.app.usage.UsageStatsManager
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.PixelFormat
import android.os.IBinder
import android.util.Log
import android.view.Gravity
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast

import com.takwolf.android.lock9.Lock9View

import java.util.ArrayList
import java.util.SortedMap
import java.util.Timer
import java.util.TimerTask
import java.util.TreeMap

class AppCheckServices : Service() {
    private var context: Context? = null
    private var timer: Timer? = null
    internal var imageView: ImageView? = null
    private var windowManager: WindowManager? = null
    private var dialog: Dialog? = null
    internal var sharedPreference: SharedPreference? = null
    internal var pakageName: MutableList<String>? = null
    private var isLockOpened = false
    private var settingPID = -1

    private val updateTask = object : TimerTask() {
        override fun run() {
            if (sharedPreference != null) {
                pakageName = ArrayList()
                pakageName!!.add(COM_ANDROID_SETTINGS)
            }
            if (!isLockOpened) {
                if (isConcernedAppIsInForeground) {
                    if (imageView != null) {
                        imageView!!.post {
                            if (!currentApp.matches(previousApp.toRegex())) {
                                isLockOpened = true
                                showDialog()
                                previousApp = currentApp
                            }
                        }
                    }
                } else {
                    if (imageView != null) {
                        imageView!!.post {
                            isLockOpened = false
                            hideUnlockDialog()
                        }
                    }
                }
            }
        }
    }

    val isConcernedAppIsInForeground: Boolean
        get() {
            val manager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            var mpackageName = manager.runningAppProcesses[0].processName
            val pid = manager.runningAppProcesses[0].pid

            val usage = context!!.getSystemService(Context.USAGE_STATS_SERVICE) as UsageStatsManager
            val time = System.currentTimeMillis()
            val stats = usage.queryUsageStats(UsageStatsManager.INTERVAL_DAILY, 0, time)
            if (stats != null) {
                val runningTask = TreeMap<Long, UsageStats>()
                for (usageStats in stats) {
                    runningTask[usageStats.lastTimeUsed] = usageStats
                }
                if (runningTask.isEmpty()) {
                    Log.d(TAG, "isEmpty Yes")
                    mpackageName = ""
                } else {
                    mpackageName = runningTask[runningTask.lastKey()]!!.packageName
                    Log.d(TAG, "isEmpty No : $mpackageName")
                }
            }


            var i = 0
            while (pakageName != null && i < pakageName!!.size) {
                if (mpackageName == pakageName!![i]) {
                    currentApp = pakageName!![i]
                    settingPID = pid
                    return true
                }
                i++
            }

            return false
        }

    override fun onCreate() {
        super.onCreate()
        context = applicationContext
        sharedPreference = SharedPreference()
        if (sharedPreference != null) {
            pakageName = ArrayList()
            pakageName!!.add(COM_ANDROID_SETTINGS)
            //pakageName = sharedPreference.getLocked(context);
        }

        timer = Timer("AppCheckServices")
        timer!!.schedule(updateTask, 1000L, 1000L)

        windowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
        imageView = ImageView(this)
        imageView!!.visibility = View.GONE


        val params: WindowManager.LayoutParams
        params = WindowManager.LayoutParams(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
            PixelFormat.TRANSLUCENT
        )


        params.gravity = Gravity.START or Gravity.TOP
        params.x = applicationContext.resources.displayMetrics.widthPixels / 2
        params.y = applicationContext.resources.displayMetrics.heightPixels / 2

        windowManager!!.addView(imageView, params)
    }

    internal fun hideUnlockDialog() {
        previousApp = ""
        try {
            if (dialog != null) {
                if (dialog!!.isShowing) {
                    dialog!!.dismiss()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    internal fun showDialog() {
        if (context == null) {
            context = applicationContext
        }
        val layoutInflater = LayoutInflater.from(context)
        val promptsView = layoutInflater.inflate(R.layout.popup_unlock, null)
        val lock9View = promptsView.findViewById<View>(R.id.lock_9_view) as Lock9View
        val close = promptsView.findViewById<View>(R.id.btn_close) as Button
        //FlatButton forgetPassword = (FlatButton) promptsView.findViewById(R.id.forgetPassword);
        lock9View.setCallBack { password ->
            if (password.matches(sharedPreference!!.getPassword(context!!)!!.toRegex())) {
                dialog!!.dismiss()
                isLockOpened = false
            } else {
                Toast.makeText(applicationContext, "Wrong Pattern Try Again", Toast.LENGTH_SHORT)
                    .show()
            }
        }

        close.setOnClickListener {
            val startMain = Intent(Intent.ACTION_MAIN)
            startMain.addCategory(Intent.CATEGORY_HOME)
            startMain.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(startMain)
        }
        /* forgetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(AppCheckServices.this, PasswordRecoveryActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                dialog.dismiss();
            }
        });
        */


        dialog = Dialog(context!!, android.R.style.Theme_Black_NoTitleBar_Fullscreen)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.setCancelable(false)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.window!!.setType(WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY)
        dialog!!.window!!.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT
        )
        dialog!!.setContentView(promptsView)
        dialog!!.window!!.setGravity(Gravity.CENTER)

        dialog!!.setOnKeyListener(object : DialogInterface.OnKeyListener {
            override fun onKey(
                dialog: DialogInterface, keyCode: Int,
                event: KeyEvent
            ): Boolean {

                if (keyCode == KeyEvent.KEYCODE_BACK && event.action == KeyEvent.ACTION_UP) {
                    val startMain = Intent(Intent.ACTION_MAIN)
                    startMain.addCategory(Intent.CATEGORY_HOME)
                    startMain.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(startMain)
                }
                return true
            }
        })

        dialog!!.show()

    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent != null) {

        }
        /* We want this service to continue running until it is explicitly
         * stopped, so return sticky.
         */
        return Service.START_STICKY
    }


    override fun onDestroy() {
        super.onDestroy()
        timer!!.cancel()
        timer = null
        if (imageView != null) {
            windowManager!!.removeView(imageView)
        }
        /**** added to fix the bug of view not attached to window manager  */
        try {
            if (dialog != null) {
                if (dialog!!.isShowing) {
                    dialog!!.dismiss()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    companion object {

        val TAG = "AppCheckServices"
        val COM_ANDROID_SETTINGS = "com.android.settings"
        var currentApp = ""
        var previousApp = ""
    }
}