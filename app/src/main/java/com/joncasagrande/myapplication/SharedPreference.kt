package com.joncasagrande.myapplication

import android.content.Context
import android.content.SharedPreferences

class SharedPreference {

    // This four methods are used for maintaining favorites.
    fun setPassword(context: Context, password: String) {
        val settings: SharedPreferences
        val editor: SharedPreferences.Editor

        settings = context.getSharedPreferences(
            AppLockConstants.MyPREFERENCES,
            Context.MODE_PRIVATE
        )
        editor = settings.edit()
        editor.putString(AppLockConstants.PASSWORD, password)
        editor.commit()
    }

    fun getPassword(context: Context): String? {
        val passwordPref: SharedPreferences
        passwordPref =
            context.getSharedPreferences(AppLockConstants.MyPREFERENCES, Context.MODE_PRIVATE)
        return if (passwordPref.contains(AppLockConstants.PASSWORD)) {
            passwordPref.getString(AppLockConstants.PASSWORD, "")
        } else ""
    }

    fun setPermission(context: Context, permission: Boolean) {
        val settings: SharedPreferences
        val editor: SharedPreferences.Editor

        settings = context.getSharedPreferences(
            AppLockConstants.MyPREFERENCES,
            Context.MODE_PRIVATE
        )
        editor = settings.edit()
        editor.putBoolean(AppLockConstants.PERMISSIONS, permission)
        editor.commit()
    }


    fun hasPermission(context: Context): Boolean {
        val passwordPref: SharedPreferences
        passwordPref =
            context.getSharedPreferences(AppLockConstants.MyPREFERENCES, Context.MODE_PRIVATE)
        return if (passwordPref.contains(AppLockConstants.PASSWORD)) {
            passwordPref.getBoolean(AppLockConstants.PERMISSIONS, false)
        } else false
    }
}