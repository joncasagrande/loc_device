package com.joncasagrande.myapplication

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Spinner
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

import java.util.ArrayList

class PasswordRecoveryActivity : AppCompatActivity() {
    internal var sharedPreferences: SharedPreferences
    internal var editor: SharedPreferences.Editor
    internal var context: Context
    internal var questionsSpinner: Spinner
    internal var answer: EditText
    internal var confirmButton: FlatButton
    internal var questionNumber = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        context = applicationContext
        setContentView(R.layout.activity_recovery_password)

        confirmButton = findViewById<View>(R.id.confirmButton) as FlatButton
        questionsSpinner = findViewById<View>(R.id.questionsSpinner) as Spinner
        answer = findViewById<View>(R.id.answer) as EditText

        sharedPreferences =
            getSharedPreferences(AppLockConstants.MyPREFERENCES, Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()


        val list = ArrayList<String>()
        list.add("Select your security question?")
        list.add("What is your pet name?")
        list.add("Who is your favorite teacher?")
        list.add("Who is your favorite actor?")
        list.add("Who is your favorite actress?")
        list.add("Who is your favorite cricketer?")
        list.add("Who is your favorite footballer?")

        val stringArrayAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, list)
        stringArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        questionsSpinner.adapter = stringArrayAdapter

        questionsSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                questionNumber = position
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }

        confirmButton.setOnClickListener {
            if (questionNumber != 0 && !answer.text.toString().isEmpty()) {
                if (sharedPreferences.getInt(
                        AppLockConstants.QUESTION_NUMBER,
                        0
                    ) == questionNumber && sharedPreferences.getString(
                        AppLockConstants.ANSWER,
                        ""
                    )!!.matches(answer.text.toString().toRegex())
                ) {
                    editor.putBoolean(AppLockConstants.IS_PASSWORD_SET, false)
                    editor.commit()
                    editor.putString(AppLockConstants.PASSWORD, "")
                    editor.commit()
                    //Intent i = new Intent(PasswordRecoveryActivity.this, PasswordSetActivity.class);
                    //startActivity(i);
                    finish()
                } else {
                    Toast.makeText(
                        applicationContext,
                        "Your question and answer didn't matches",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            } else {
                Toast.makeText(
                    applicationContext,
                    "Please select a question and write an answer",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

    }

    override fun onBackPressed() {
        super.onBackPressed()
    }


    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onStop() {
        super.onStop()
        super.onStop()
    }
}