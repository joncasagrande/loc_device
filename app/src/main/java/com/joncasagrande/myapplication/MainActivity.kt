package com.joncasagrande.myapplication



import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.net.Uri
import android.os.Build
import androidx.annotation.RequiresApi

class MainActivity : AppCompatActivity() {
    val sharedPreference = SharedPreference()

    val OVERLAY_CODE = 1010
    val USAGE_CODE = 1011

    var overlayPermission = false
    var permissionUsage = false
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        sharedPreference.setPassword(this,"1236")

        if(!sharedPreference.hasPermission(this)) {
            checkDrawOverlayPermission()
            checkpermissionUsage()
        }else{
            startMonitorService()
        }

    }

    fun savePermissionPref(){
        if(permissionUsage && overlayPermission){
            sharedPreference.setPermission(this,true)
        }
    }

    fun startMonitorService() = startService(Intent(this, AppCheckServices::class.java))

    fun checkpermissionUsage(){
        startActivityForResult(Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS),USAGE_CODE)
    }

    fun checkDrawOverlayPermission() {
        if (!Settings.canDrawOverlays(this)) {
            val intent = Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                Uri.parse("package:" + getPackageName()))
            startActivityForResult(intent, OVERLAY_CODE)
        }else{
            startMonitorService()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == OVERLAY_CODE) {
            if (Settings.canDrawOverlays(this)) {
                overlayPermission = true
                startMonitorService()
                savePermissionPref()

            }
        }else if (requestCode == USAGE_CODE){
            permissionUsage = true
            savePermissionPref()
        }
    }


}

