package com.joncasagrande.myapplication

import android.app.Application

class AppLockApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        appInstance = this
    }

    enum class TrackerName {
        APP_TRACKER, // Tracker used only in this app.
        GLOBAL_TRACKER // Tracker used by all the apps from a company. eg:
        // roll-up tracking.
    }

    companion object {

        val PROPERTY_ID = "UA-62504955-1"
        private var appInstance: AppLockApplication? = null
    }

}