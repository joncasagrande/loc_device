package com.joncasagrande.myapplication

interface AppLockConstants {
    companion object {

        val MyPREFERENCES = "MyPreferences"
        val IS_PASSWORD_SET = "is_password_set"
        val PERMISSIONS = "hasPermissions"
        val PASSWORD = "password"
        val ANSWER = "answer"
        val QUESTION_NUMBER = "question_number"
        val LOCKED = "locked"
    }


}
